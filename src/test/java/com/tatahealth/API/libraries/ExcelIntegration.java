package com.tatahealth.API.libraries;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;

import com.tatahealth.API.Billing.Login;
import com.tatahealth.API.Billing.PaymentDetails;


public class ExcelIntegration extends Login{
	
	 private static String[] Appointment_Details_column = {"PaymentMode", "SlotId","ConsumerName", " Service Amount","Additional Service Amount","Service Discount","Additional Service Discount","Paid"};
	 
	 private Workbook workbook = null;
	 private Sheet appointment_Details_Sheet = null;
	 private CellStyle headerCellStyle = null;
	 
	 private Font headerFont = null;
	 
	 private  static Integer rowNonTDH;
	 
	
	public void createSheet() throws IOException{
		//create workbook
	    workbook = new XSSFWorkbook();
		// Create a Font for styling header cells
       headerFont = workbook.createFont();
       headerFont.setBold(true);
       headerFont.setFontHeightInPoints((short) 10);
       headerFont.setColor(IndexedColors.BLACK.getIndex());
		
       
       // Create a CellStyle with the font
       headerCellStyle = workbook.createCellStyle();
       headerCellStyle.setFont(headerFont);
       headerCellStyle.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
       headerCellStyle.setFillPattern(FillPatternType.BIG_SPOTS);
    //   headerCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
       
       
		 /* CreationHelper helps us create instances of various things like DataFormat, 
        Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
		CreationHelper createHelper = workbook.getCreationHelper();

		//create a sheet for Appointment Details
				appointment_Details_Sheet = workbook.createSheet("Appointment_Details");
		
		
		
	}
	
	
	public void appointment_details_header(String testcaseFlow) throws Exception{
		rowNonTDH = 0;   
		// Create a Row
        Row headerRow = appointment_Details_Sheet.createRow(rowNonTDH);
        Cell cell = headerRow.createCell(0);
        cell.setCellStyle(headerCellStyle);
        cell.setCellValue(testcaseFlow);//no service, additional service, add
        
        
        //cellstyle for 2 row
        headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        
        //to add next row
        rowNonTDH++;
   
        Row vendor_row = appointment_Details_Sheet.createRow(rowNonTDH);
        // Create cells
        for(int i = 0; i < Appointment_Details_column.length; i++) {
            Cell vendor_cell = vendor_row.createCell(i);
            vendor_cell.setCellValue(Appointment_Details_column[i]);
            vendor_cell.setCellStyle(headerCellStyle);
            
        }
        rowNonTDH++;
        for(int i = 0; i < Appointment_Details_column.length; i++) {
        	appointment_Details_Sheet.autoSizeColumn(i);
        }
        
       FileOutputStream fileOut = new FileOutputStream("./appointment_details.xlsx");
       workbook.write(fileOut);
       fileOut.flush();
       fileOut.close();
	}
	
	public void appointment_services(Map<String, PaymentDetails> map) throws Exception{
		for(Map.Entry<String, PaymentDetails> entry:map.entrySet()) {
			Row payment_row = appointment_Details_Sheet.createRow(rowNonTDH); 
			PaymentDetails p = entry.getValue();
			 payment_row.createCell(0).setCellValue(p.getModeOfPayment());
		     payment_row.createCell(1).setCellValue(p.getSlotId());
		     payment_row.createCell(2).setCellValue(p.getAppointmentName());
		     payment_row.createCell(3).setCellValue(p.getAmount());
		     payment_row.createCell(4).setCellValue(p.getAdditionalServiceAmount());
		     payment_row.createCell(5).setCellValue(p.getServiceDiscount());
		     payment_row.createCell(6).setCellValue(p.getAdditionalDiscount());
		     payment_row.createCell(7).setCellValue(p.getPaid());
		}

	     for(int i = 0; i < Appointment_Details_column.length; i++) {
	    	 appointment_Details_Sheet.autoSizeColumn(i);
	      }
	        
	       FileOutputStream fileOut = new FileOutputStream("./appointment_details.xlsx");
	       workbook.write(fileOut);
	       fileOut.flush();
	       fileOut.close();
	      workbook.close();
	}
	
	
     public void deleteFile() throws Exception{
 		 File file = new File("./appointment_details.xlsx"); 
 		if(file.exists())
 		{
 		    file.delete();
 			System.out.println("File deleted successfully"); 
 		}
 		
 		else
 		{
 			Assert.assertTrue(false, "File Not deleted");
 		}
 	     }
	public void readExcel() throws Exception{
		String file = "./appointment_details.xlsx";
		FileInputStream stream = new FileInputStream(new File(file));
		Workbook book = new XSSFWorkbook(stream);
		int sheetRow = 1;
		Sheet sheetNum = book.getSheetAt(sheetRow);
		Iterator<Row> iterRow = sheetNum.iterator();
		//iterRow.next();
		iterRow.next();
		iterRow.next();
		while(iterRow.hasNext()) {
			PaymentDetails p = new PaymentDetails();
			Row row = iterRow.next();
			Iterator<Cell> iterCell = row.iterator();
			Cell cellNum = null;
			
			//read mode of payment
			cellNum = iterCell.next();
			p.setModeOfPayment(cellNum.getStringCellValue());
			String mop = cellNum.getStringCellValue();
			//System.out.println("mop : "+mop);
			
			//read appointment id
			cellNum = iterCell.next();
			p.setAppointmentId(cellNum.getStringCellValue());
			
			//read bill id
			cellNum = iterCell.next();
			int bill = (int)cellNum.getNumericCellValue();
			p.setBillId(bill);
			
			//read amount
			cellNum = iterCell.next();
			int amount  = (int)cellNum.getNumericCellValue();
			p.setAmount(amount);
			
			//read additional service amount
			cellNum = iterCell.next();
			amount  = (int)cellNum.getNumericCellValue();
			p.setAdditionalServiceAmount(amount);
			
			//read service discount
			cellNum = iterCell.next();
			amount  = (int)cellNum.getNumericCellValue();
			p.setServiceDiscount(amount);
			
			//read additional service discount
			cellNum = iterCell.next();
			amount  = (int)cellNum.getNumericCellValue();
			p.setAdditionalServiceAmount(amount);
			
			//read paid
			cellNum = iterCell.next();
			p.setPaid(cellNum.getStringCellValue());
			readExcel.put(mop, p);
			stream.close();
			book.close();
			
		}
	}	
	
}
