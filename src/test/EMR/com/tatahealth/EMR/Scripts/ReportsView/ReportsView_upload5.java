package com.tatahealth.EMR.Scripts.ReportsView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.pages.ReportsView.ReportsView;

public class ReportsView_upload5 {
	ReportsView report = new ReportsView();
	public static ExtentTest logger;
	public static String UHID;
	Random random = new Random();
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "reportsViewUpload";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}
	public synchronized void reportsViewUploadLunch() throws Exception {
		logger = Reports.extent.createTest("EMR_Reports View/Upload Page");
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
		report.getEmrTopMenuElement(Login_Doctor.driver, logger, "Reports").click();
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
	}
	@Test(priority = 42, groups ={ "Regression", "ReportView" })
	public void specialCharactersInToDate() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter special char in To Date field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String special = "^((?=[A-Za-z@])(?![_\\\\\\\\-]).)*$";
		report.getToDateField(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		report.getToDateField(Login_Doctor.driver, logger).sendKeys(special);
		if (!Pattern.matches("[^((?=[A-Za-z0-9@])(?![_\\\\\\\\-]).)*$]",
				report.getToDateField(Login_Doctor.driver, logger).getText())) {
			Assert.assertTrue(true, "Do not allow special characters");
		} else {
			Assert.assertTrue(false, "Allow special characters");
		}
	}
	@Test(priority = 43, groups ={ "Regression", "ReportView" })
	public void searchPatientOnlyWithToDate() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to search patient only with To Date");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		report.getToDateField(Login_Doctor.driver, logger).click();
		report.getToDateYear(Login_Doctor.driver, logger).click();
		report.getToDateDay(Login_Doctor.driver, logger).click();
		report.getEmrSeachPatientBtn(Login_Doctor.driver, logger).click();
		Assert.assertTrue(report.getFromDatePopupMessage(Login_Doctor.driver, logger).isDisplayed(), "Pop up message displayed!!");
	}
	@Test(priority = 44, groups ={ "Regression", "ReportView" })
	public void searchPatientWithFutureToDate() throws Exception {
		logger = Reports.extent.createTest("To check when user search patient with To Date which is not registered");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		report.getToDateField(Login_Doctor.driver, logger).click();
		report.getFutureToDate(Login_Doctor.driver, logger).click();
		if (report.getToDateField(Login_Doctor.driver, logger).getAttribute("text")==null) {
			Assert.assertTrue(true, "Future Date cannot be selected!!!");
		}
		else {
			Assert.assertTrue(false);
		}	
	}
	@Test(priority = 45, groups ={ "Regression", "ReportView" })
	public void searchPatientWithCurrentToDate() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to select current, past and future date");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		report.getToDateField(Login_Doctor.driver, logger).click();
		report.getCurrentToDate(Login_Doctor.driver, logger).click();
		if (report.getToDateField(Login_Doctor.driver, logger).getText()!=null) {
			Assert.assertTrue(true, "Future Date cannot be selected!!!");
		}
		else {
			Assert.assertTrue(false);
		}	
	}
	@Test(priority = 46, groups ={ "Regression", "ReportView" })
	public void searchPatientWithToDateLessThenFromDate() throws Exception {
		logger = Reports.extent.createTest("Check whether Visit To Date can be lesser than visit from date");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		report.getFromDateField(Login_Doctor.driver, logger).click();
		report.getCurrentFromDate(Login_Doctor.driver, logger).click();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		Date previous = calendar.getTime();
		String previousDate=dateFormat.format(previous).toString();
		String newString = previousDate.replace("/", "-");
		report.getToDateField(Login_Doctor.driver, logger).sendKeys(newString);
		if (report.getToDateField(Login_Doctor.driver, logger).getAttribute("text")==null) {
			Assert.assertTrue(true, "To Date cannot be lesser then visit date");
		}
		else {
			Assert.assertTrue(false, "To Date can be lesser then visit date");
		}
	}
	@Test(priority = 47, groups = { "Regression", "ReportView" })
	public void verifyResetBtn() throws Exception {
		logger = Reports.extent.createTest("To check whether user can be able to reset the search detail");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String mobileNumber = RandomStringUtils.randomNumeric(10);
		String UHID = RandomStringUtils.randomNumeric(5);
		String name = RandomStringUtils.randomAlphabetic(6);	
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys(UHID);
		report.getNameField(Login_Doctor.driver, logger).sendKeys(name);
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(mobileNumber);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(3);
		report.getResetBtn(Login_Doctor.driver, logger).click();		
		if (report.getUHIDField(Login_Doctor.driver, logger).getAttribute("text")==null && report.getNameField(Login_Doctor.driver, logger).getAttribute("text")==null && report.getMobileField(Login_Doctor.driver, logger).getAttribute("text")==null) {
			Assert.assertTrue(true, "Reset done");
		}else {
			Assert.assertTrue(false, "Reset not done");
		}	
	}
	@Test(priority = 48, groups = { "Regression", "ReportView" })
	public void verifyResetWithEmptyField() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter more than 10 digits on mobile field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		boolean actualValue = report.getResetBtn(Login_Doctor.driver, logger).isEnabled();
		if (actualValue==true) {
			report.getResetBtn(Login_Doctor.driver, logger).click();
			Assert.assertTrue(true, "Reset button clicked");
		}
		else {
			Assert.assertTrue(false, "Reset button not clicked");
		}
	}
	@Test(priority = 49, groups = { "Regression", "ReportView" })
	public void verifySearchBtntWithEmptyField() throws Exception {
		logger = Reports.extent.createTest("To check when user click on search button with empty data");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String expectedPage = Login_Doctor.driver.getCurrentUrl();
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		String actualPage = Login_Doctor.driver.getCurrentUrl();
		try {
			Assert.assertEquals(actualPage, expectedPage);
		} catch (Exception e) {
			e.getStackTrace();
		}	
	}
}
