package com.tatahealth.EMR.Scripts.PatientAdvanceSearch;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.tatahealth.EMR.pages.PatientAdvanceSearch.AdvancePatientSearch;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;

public class PatientAdvanceSearch2 {

	public static ExtentTest logger;
	AdvancePatientSearch adPatSearch = new AdvancePatientSearch();

	private static String input;
	@BeforeClass(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	@Parameters("role")
	public static void beforeClass(String role) throws Exception {
		Login_Doctor.executionName="Patient Advance Search";
		Login_Doctor.LoginTestwithDiffrentUser(role);
		AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger).click();

	}

	@AfterClass(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public static void afterClass() {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}

	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch01() {
		logger = Reports.extent.createTest("Verifying the Advance Search Button on HomePage");
		AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger).isDisplayed();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch03() throws InterruptedException {
		logger = Reports.extent.createTest("Verifying if the Search Page is Open");
		Web_GeneralFunctions.isVisible(adPatSearch.getAdvanceSearchLabel(Login_Doctor.driver, logger),Login_Doctor.driver);
		adPatSearch.getSearchBtn(Login_Doctor.driver, logger).isDisplayed();
	}

	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch04() {
		
		logger = Reports.extent.createTest("Verifying fields in the Advance Search Page");

		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "Name").isDisplayed();
		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "Age").isDisplayed();
		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "Dob").isDisplayed();
		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "Gender").isDisplayed();
		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "UHID").isDisplayed();
		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "Mobile").isDisplayed();
		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "Clinic ID").isDisplayed();
		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "Address").isDisplayed();
		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "Visit From Date").isDisplayed();
		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "Visit To Date").isDisplayed();
		adPatSearch.getSearchLabels(Login_Doctor.driver, logger, "Govt ID").isDisplayed();
	}

	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch05() throws Exception {
		logger = Reports.extent.createTest("Verifying if the characters can be entered in Name field.");
		adPatSearch.getSearchNameText(Login_Doctor.driver, logger).clear();
		input = RandomStringUtils.randomAlphabetic(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchNameText(Login_Doctor.driver, logger), input,"Entering random alphabets", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getSearchNameText(Login_Doctor.driver, logger),input));
	}

	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch06() throws Exception {
		logger = Reports.extent.createTest("Verifying if the numeric can be entered in Name field.");
		adPatSearch.getSearchNameText(Login_Doctor.driver, logger).clear();
		input= RandomStringUtils.randomNumeric(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchNameText(Login_Doctor.driver, logger), input,"Entering random numbers", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getSearchNameText(Login_Doctor.driver, logger),input));
	}

	@Test(dataProvider = "Patient name search data",groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch07(String name) throws Exception {
		logger = Reports.extent.createTest("Verifying if the special characters can be entered in Name field");
		adPatSearch.getSearchNameText(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchNameText(Login_Doctor.driver, logger), name,"Entering special characters" + name, Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getSearchNameText(Login_Doctor.driver, logger),name));
	}

	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch08() throws Exception {
		logger = Reports.extent.createTest("Verifying if the valid Name entered gives results.");
		adPatSearch.getSearchNameText(Login_Doctor.driver, logger).clear();
		input=SheetsAPI.getDataProperties(Web_Testbase.input + ".PatientName");

		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchNameText(Login_Doctor.driver, logger), input,"Entering value" + input, Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getSearchNameText(Login_Doctor.driver, logger),input));
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		List<WebElement> resultNames = adPatSearch.getPatientNameSearchResultSelect(Login_Doctor.driver, logger);
		boolean res;
		if (input.matches(".*[A-Z].*")) {
			String []names=input.split("(?=[A-Z])");
			for (WebElement name : resultNames) {
				res=name.getText().toLowerCase().contains(names[0].toLowerCase())||name.getText().toLowerCase().contains(names[1].toLowerCase());
				Assert.assertTrue(res,"List of Element Searched with Valid Name");
			}
		}
		else {
		for (WebElement name : resultNames) {
			Assert.assertTrue(name.getText().toLowerCase().contains(input.toLowerCase()),"List of Element Searched with Valid Name");
		}
		}	

		adPatSearch.getSearchNameText(Login_Doctor.driver, logger).clear();
	}

	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch09() throws Exception {
		logger = Reports.extent.createTest("Verifying No results found error message.");
		input=SheetsAPI.getDataProperties(Web_Testbase.input + ".InvalidName");
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchNameText(Login_Doctor.driver, logger), input,"Entering random alphabets", Login_Doctor.driver, logger);
		validateNoResultErrorMessage();
		adPatSearch.getSearchNameText(Login_Doctor.driver, logger).clear();
		}

	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch10() throws Exception {
		logger = Reports.extent.createTest("Verifying if the characters can be entered in Age Field");
		adPatSearch.getSearchAgeText(Login_Doctor.driver, logger).clear();
		input= RandomStringUtils.randomAlphabetic(3);
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchAgeText(Login_Doctor.driver, logger), input,"Entering Alphabets ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getSearchAgeText(Login_Doctor.driver, logger),input));
	}

	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch11() throws Exception {
		logger = Reports.extent.createTest("Verifying if the number can be entered in Age Field.");
		adPatSearch.getSearchAgeText(Login_Doctor.driver, logger).clear();
		input = RandomStringUtils.randomNumeric(3);
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchAgeText(Login_Doctor.driver, logger), input,"Entering numbers", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getSearchAgeText(Login_Doctor.driver, logger),input));
	}

	@Test(dataProvider = "Patient search data",groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch12(String age) throws Exception {
		logger = Reports.extent.createTest("Verifying if the  special characters can be entered in Age Field.");
		adPatSearch.getSearchAgeText(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchAgeText(Login_Doctor.driver, logger), age,"Entering special characters" + age, Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getSearchAgeText(Login_Doctor.driver, logger),age));
	}

	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch13() throws Exception {
		logger = Reports.extent.createTest("Verifying if age can be entered retrieves valid results.");
		adPatSearch.getSearchAgeText(Login_Doctor.driver, logger).clear();
		input=SheetsAPI.getDataProperties(Web_Testbase.input + ".Age");
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchAgeText(Login_Doctor.driver, logger), input,"Entering Proper age value", Login_Doctor.driver, logger);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getSearchAgeText(Login_Doctor.driver, logger),input));
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);

		List<WebElement> resultAge = adPatSearch.getPatientAgeSearchResultSelect(Login_Doctor.driver, logger);
		for (WebElement age : resultAge) {
			Assert.assertTrue(age.getText().contains(input), "List of Element Searched with Valid Age");
		}
		adPatSearch.getSearchAgeText(Login_Doctor.driver, logger).clear();
	}

	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch14() throws Exception {
		logger = Reports.extent.createTest("Verifying if No result found error message is found in Age Field.");
		input="2"+RandomStringUtils.randomNumeric(2);
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchAgeText(Login_Doctor.driver, logger),input, "Entering random alphabets", Login_Doctor.driver, logger);
		validateNoResultErrorMessage();
        adPatSearch.getSearchAgeText(Login_Doctor.driver, logger).clear();
	}

	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch15(){
		logger = Reports.extent.createTest("Verifying the limit of numbers in Age Field is 3");
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchAgeText(Login_Doctor.driver, logger),RandomStringUtils.randomNumeric(10), "Entering random alphabets", Login_Doctor.driver, logger);
		int ageEntered = adPatSearch.getSearchAgeText(Login_Doctor.driver, logger).getAttribute("value").length();
		Assert.assertTrue(ageEntered == 3, "Limit of numbers added in Age Field is 3");
		adPatSearch.getSearchAgeText(Login_Doctor.driver, logger).clear();
	}

	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch84() {
		logger = Reports.extent.createTest("Verifying the limit of characters in Name Field is 40");
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchNameText(Login_Doctor.driver, logger),RandomStringUtils.randomAlphabetic(50), "Entering random alphabets", Login_Doctor.driver, logger);
		int alphacount = adPatSearch.getSearchNameText(Login_Doctor.driver, logger).getAttribute("value").length();
		Assert.assertTrue(alphacount == 40, "Limit of numbers added in Name Field is 40");
		adPatSearch.getSearchNameText(Login_Doctor.driver, logger).clear();
	}

	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch16() throws Exception {
		logger = Reports.extent.createTest("Verifying the DOB field");
		 Web_GeneralFunctions.click(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),"Clicking on DOB Text Field", Login_Doctor.driver, logger);
		 adPatSearch.getDOBDatePicker(Login_Doctor.driver, logger).isDisplayed();
		 
		 //Current date
		 Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
	     String currday = adPatSearch.getDOBTextField(Login_Doctor.driver, logger).getAttribute("value");
		 Assert.assertTrue(currday.equals(adPatSearch.getCurrSysDate()), "Verifying if the current date is selected");
		 Web_GeneralFunctions.wait(3); 
		 adPatSearch.getDOBTextField(Login_Doctor.driver, logger).clear();
		 
		 //Futuredate
		 Web_GeneralFunctions.click(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),"Clicking on DOB Text Field", Login_Doctor.driver, logger);
		 Web_GeneralFunctions.wait(3);
		 String futuredateclassname=adPatSearch.getFutureDate(Login_Doctor.driver, logger).getAttribute("class");
		 Assert.assertTrue(futuredateclassname.contains("disabled"), "Verifying if the future date object is disabled");
		 
		
		 //Past month,date,year
		 adPatSearch.getDOBTextField(Login_Doctor.driver, logger).clear();
		 Web_GeneralFunctions.wait(3); 

		 input=SheetsAPI.getDataProperties(Web_Testbase.input + ".PastDate");
		 adPatSearch.selectPastDate(input,Login_Doctor.driver, logger);
		 Web_GeneralFunctions.wait(3); 
		 String pastDate=adPatSearch.getDOBTextField(Login_Doctor.driver,logger).getAttribute("value");
		 Assert.assertTrue(pastDate.equals(adPatSearch.getFormattedDate(input)),"Past Date,Month and Year Selected");
		Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger), "Clicking to go back to advance search page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
	}
	
	//Disabling this test case because intermittently current date is getting appended
	@Test(enabled=false,groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch17() throws Exception {
		logger = Reports.extent.createTest("Verifying the DOB field with characters");
		Web_GeneralFunctions.click(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),"Clicking on DOB Text Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(adPatSearch.getDOBTextField(Login_Doctor.driver, logger), "Clearing the DOB Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		input= RandomStringUtils.randomAlphabetic(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getDOBTextField(Login_Doctor.driver, logger), input,"Entering Alphabets ", Login_Doctor.driver, logger);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),input),"Verifying if the characters are entered");

		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String enteronefield = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter atleast one field".equals(enteronefield.trim()),"Actual Toast message with expected");
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),input),"Verifying the empty date field");
	}
	
	//Disabling this test case because intermittently current date is getting appended
	@Test(enabled=false,groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch18() throws Exception {
		
		logger = Reports.extent.createTest("Verifying the DOB field with numeric");
		Web_GeneralFunctions.click(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),"Clicking on DOB Text Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(adPatSearch.getDOBTextField(Login_Doctor.driver, logger), "Clearing the DOB Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		input= RandomStringUtils.randomNumeric(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getDOBTextField(Login_Doctor.driver, logger), input,"Entering numbers", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),input),"Verifying if the numbers are entered");

		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String enteronefield = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter atleast one field".equals(enteronefield.trim()),"Actual Toast message with expected");
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),input),"Verifying the empty date field");
	}
	
	//Disabling this test case because intermittently current date is getting appended
	@Test(enabled=false,groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch19() throws Exception {
		logger = Reports.extent.createTest("Verifying the DOB field with special characters");
		Web_GeneralFunctions.click(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),"Clicking on DOB Text Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(adPatSearch.getDOBTextField(Login_Doctor.driver, logger), "Clearing the DOB Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		input= SheetsAPI.getDataProperties(Web_Testbase.input + ".SpecialChars");
		Web_GeneralFunctions.sendkeys(adPatSearch.getDOBTextField(Login_Doctor.driver, logger), input,"Entering special characters ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),input),"Verifying if the special characters are entered");

		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String enteronefield = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter atleast one field".equals(enteronefield.trim()),"Actual Toast message with expected");
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),input),"Verifying the empty date field");
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch20() throws Exception {
		logger = Reports.extent.createTest("Verifying the Details with DOB field search");
		Web_GeneralFunctions.clear(adPatSearch.getDOBTextField(Login_Doctor.driver, logger), "Clearing the DOB Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),"Clicking on DOB Text Field", Login_Doctor.driver, logger);
		input=SheetsAPI.getDataProperties(Web_Testbase.input + ".DOB");
		adPatSearch.selectPastDate(input,Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getViewDetailsBtn(Login_Doctor.driver, logger), "Clicking on view details", Login_Doctor.driver, logger);
		String patientbdate=adPatSearch.getPatientDOB(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patientbdate.equals(adPatSearch.getFormattedDate(input)), "Verifying the birthday with search result");
		Web_GeneralFunctions.click(adPatSearch.getEmrMenu(Login_Doctor.driver, logger), "Clicking on Main menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getModuleFromLeftMenu(Login_Doctor.driver, logger,"Appointments"), "Clicking on Appointments", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger), "Clicking to go back to advance search page", Login_Doctor.driver, logger);
		
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch21() throws Exception {
		logger = Reports.extent.createTest("Verifying the No Results found for DOB field");
		Web_GeneralFunctions.click(adPatSearch.getDOBTextField(Login_Doctor.driver, logger),"Clicking on DOB Text Field", Login_Doctor.driver, logger);
		adPatSearch.getDOBDatePicker(Login_Doctor.driver, logger).isDisplayed();
		 
		 //Current date
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		validateNoResultErrorMessage();
		adPatSearch.getDOBTextField(Login_Doctor.driver, logger).clear();	
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch22() throws Exception {
		logger = Reports.extent.createTest("Verifying the Gender Field");
		Web_GeneralFunctions.click(adPatSearch.getGender(Login_Doctor.driver, logger),"Clicking on gender Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		adPatSearch.getGender(Login_Doctor.driver, logger).isDisplayed();		
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch23() throws Exception {
		logger = Reports.extent.createTest("Verifying the Gender Field Options");
		Web_GeneralFunctions.click(adPatSearch.getGender(Login_Doctor.driver, logger),"Clicking on gender Field", Login_Doctor.driver, logger);
		adPatSearch.getGender(Login_Doctor.driver, logger).isDisplayed();	
		Web_GeneralFunctions.wait(3);
		List<WebElement> l1=adPatSearch.getGenderDropdown(Login_Doctor.driver, logger);
		List<String>actualList=new ArrayList<>();
		for(WebElement w1:l1 )
			actualList.add(w1.getText());
		List<String>expectedList=Arrays.asList("Select","Male","Female");
		Assert.assertTrue(actualList.equals(expectedList), "Verifying the dropdown options");
	}
	

	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch24() throws Exception {
		logger = Reports.extent.createTest("Verifying the Gender Field selects only 1 value");
		Web_GeneralFunctions.click(adPatSearch.getGender(Login_Doctor.driver, logger),"Clicking on gender Field", Login_Doctor.driver, logger);
		adPatSearch.getGender(Login_Doctor.driver, logger).isDisplayed();	
		input=SheetsAPI.getDataProperties(Web_Testbase.input + ".Gender");
		
		adPatSearch.getPatientGenderDropdown(Login_Doctor.driver, logger, input);
		Web_GeneralFunctions.wait(3);
		Assert.assertEquals("Female", adPatSearch.getSelectedGenderOption(Login_Doctor.driver,logger).getText());
		Web_GeneralFunctions.click(adPatSearch.getGender(Login_Doctor.driver, logger),"Clicking on gender Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		adPatSearch.getPatientGenderDropdown(Login_Doctor.driver, logger, "Select");
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch25() throws Exception {
		logger = Reports.extent.createTest("Verifying the Alphabets can be entered in UHID Field");
		input = RandomStringUtils.randomAlphabetic(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger), input,"Entering Alphabets ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger),input));
		adPatSearch.getUHIDTextField(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch26() throws Exception {
		logger = Reports.extent.createTest("Verifying the Numbers can be entered in UHID Field");
		input = RandomStringUtils.randomNumeric(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger), input,"Entering numbers", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger),input));
		adPatSearch.getUHIDTextField(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch27() throws Exception {
		logger = Reports.extent.createTest("Verifying the special characters can be entered in UHID Field");
		input= SheetsAPI.getDataProperties(Web_Testbase.input + ".SpecialChars");
		Web_GeneralFunctions.sendkeys(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger), input,"Entering special characters ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger),input));
		adPatSearch.getUHIDTextField(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch28() throws Exception {
		logger = Reports.extent.createTest("Verifying the valid UHID can be entered in UHID Field");
		input= SheetsAPI.getDataProperties(Web_Testbase.input + ".UHID");
		Web_GeneralFunctions.sendkeys(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger), input,"Entering Alphabets ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.getUHIDDetail(Login_Doctor.driver, logger).getText().contains(input),"Verifying if the result has same UHID like the entered value");
		adPatSearch.getUHIDTextField(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch29() throws Exception {
		logger = Reports.extent.createTest("Verifying the numbers can be entered in UHID Field");
		input = RandomStringUtils.randomNumeric(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger), input,"Entering Numbers ", Login_Doctor.driver, logger);
		validateNoResultErrorMessage();
		adPatSearch.getUHIDTextField(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch30() throws Exception {
			logger = Reports.extent.createTest("Verifying the limit of number in UHID Field is 10");
			input= RandomStringUtils.randomNumeric(20);
			Web_GeneralFunctions.sendkeys(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger), input,"Entering Numbers with 20 alphabets ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			int uhidEntered = adPatSearch.getUHIDTextField(Login_Doctor.driver, logger).getAttribute("value").length();
			Assert.assertTrue(uhidEntered == 10, "Limit of numbers input in UHID field is 10");
			adPatSearch.getUHIDTextField(Login_Doctor.driver, logger).clear();
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch31() throws Exception {
			logger = Reports.extent.createTest("Verifying the alphabets input in Mobile field");
			input= RandomStringUtils.randomAlphabetic(10);
			Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), input,"Entering Alphabets ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getMobileField(Login_Doctor.driver, logger),input));
			adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
			}
	
	@Test
	public synchronized void advanceSearch32() throws Exception {
			logger = Reports.extent.createTest("Verifying the numbers input in Mobile field");
			input= RandomStringUtils.randomNumeric(10);
			Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), input,"Entering Numbers ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getMobileField(Login_Doctor.driver, logger),input));
			adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch33() throws Exception {
			logger = Reports.extent.createTest("Verifying the special character input in Mobile field");
			input= SheetsAPI.getDataProperties(Web_Testbase.input + ".SpecialChars");
			Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), input,"Entering special character ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getMobileField(Login_Doctor.driver, logger),input));
			adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch34() throws Exception {
			logger = Reports.extent.createTest("Verifying the valid number input in Mobile field");
			input= SheetsAPI.getDataProperties(Web_Testbase.input + ".MobileNumber");
			Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), input,"Entering special character ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getMobileField(Login_Doctor.driver, logger),input));
			Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			
			boolean res;
			List<WebElement> results=adPatSearch.getResultContainer(Login_Doctor.driver, logger);
			for(int i=0;i<results.size();i++)
			{
	
				List<WebElement>phonenum=adPatSearch.getMobileNoFromSearchResult(i+1,Login_Doctor.driver, logger);
				if(phonenum==null)
				{
					Assert.fail("No results are found");
				}
				else if(phonenum.size()==1)
					Assert.assertTrue(phonenum.get(0).getText().contains(input));
				else
				{
				res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input);
				Assert.assertTrue(res);
				}
			}
			adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
		}
	
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch35() throws Exception {
			logger = Reports.extent.createTest("Verifying the nonreg mobile input in Mobile field");
			input= SheetsAPI.getDataProperties(Web_Testbase.input + ".InvalidPhNo");
			Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), input,"Entering special character ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getMobileField(Login_Doctor.driver, logger),input));
			validateNoResultErrorMessage();
			adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
		
		}
	
	//Test Case AD _Search_36 and AD _Search_38 are covered as part of this test case
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch36() throws Exception {
			logger = Reports.extent.createTest("Verifying the number length in Mobile field");
			Web_GeneralFunctions.click(adPatSearch.getMobileField(Login_Doctor.driver, logger),"Clicking on mobile number Field", Login_Doctor.driver, logger);
			input= RandomStringUtils.randomNumeric(20);
			Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), input,"Entering special character ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			int alphaCount = adPatSearch.getMobileField(Login_Doctor.driver, logger).getAttribute("value").length();
			Assert.assertTrue(alphaCount == 10, "Limit of numbers added in Mobile Field is 10");
			adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();	
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch37() throws Exception {
			logger = Reports.extent.createTest("Verifying the search if <10 digit input in Mobile field");
			input= SheetsAPI.getDataProperties(Web_Testbase.input + ".InvalidSample");
			Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), input,"Entering special character ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			int numcount = adPatSearch.getMobileField(Login_Doctor.driver, logger).getAttribute("value").length();
			Assert.assertTrue(numcount<10, "Length of mobile number entered "+numcount);
			Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			
			boolean res;
			List<WebElement> results=adPatSearch.getResultContainer(Login_Doctor.driver, logger);
			for(int i=0;i<results.size();i++)
			{
				List<WebElement>phonenum=adPatSearch.getMobileNoFromSearchResult(i+1,Login_Doctor.driver, logger);
				if(phonenum==null)
				{
					Assert.fail("No results are found");
				}
				else if(phonenum.size()==1)
					Assert.assertTrue(phonenum.get(0).getText().contains(input));
				else
				{
				res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input);
				Assert.assertTrue(res);
				}
			}
			adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch39() throws Exception {
			logger = Reports.extent.createTest("Verifying the characters in Clinic ID field");
			input= RandomStringUtils.randomAlphabetic(10);
			Web_GeneralFunctions.sendkeys(adPatSearch.getClinicID(Login_Doctor.driver, logger), input,"Entering Alphabets", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getClinicID(Login_Doctor.driver, logger),input),"Input characters in clinicID field");
			adPatSearch.getClinicID(Login_Doctor.driver, logger).clear();
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch40() throws Exception {
			logger = Reports.extent.createTest("Verifying the numbers in Clinic ID field");
			input= RandomStringUtils.randomNumeric(10);
			Web_GeneralFunctions.sendkeys(adPatSearch.getClinicID(Login_Doctor.driver, logger), input,"Entering Numbers", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getClinicID(Login_Doctor.driver, logger),input),"Input numbers in clinicID field");
			adPatSearch.getClinicID(Login_Doctor.driver, logger).clear();
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch41() throws Exception {
			logger = Reports.extent.createTest("Verifying the special characters in Clinic ID field");
			input= SheetsAPI.getDataProperties(Web_Testbase.input + ".SpecialChars");
			Web_GeneralFunctions.sendkeys(adPatSearch.getClinicID(Login_Doctor.driver, logger), input,"Entering Special characters", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getClinicID(Login_Doctor.driver, logger),input),"Input special characters in clinicID field");
			adPatSearch.getClinicID(Login_Doctor.driver, logger).clear();
		}
	

	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch42() throws Exception {
			logger = Reports.extent.createTest("Verifying the valid in Clinic ID field");
			input=SheetsAPI.getDataProperties(Web_Testbase.input + ".ClinicID");
			Web_GeneralFunctions.sendkeys(adPatSearch.getClinicID(Login_Doctor.driver, logger), input,"Entering Clinic ID", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.click(adPatSearch.getViewDetailsBtn(Login_Doctor.driver, logger), "Clicking on view details", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			String actual=adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
			Assert.assertTrue(actual.equals(input), "Verifying the clinic ID with search result");
			Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger), "Clicking to go back to advance search page", Login_Doctor.driver, logger);
		
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch43() throws Exception {
			logger = Reports.extent.createTest("Verifying the No result found for Clinic ID field");
			input= RandomStringUtils.randomAlphanumeric(40);
			Web_GeneralFunctions.sendkeys(adPatSearch.getClinicID(Login_Doctor.driver, logger), input,"No result found ", Login_Doctor.driver, logger);
			validateNoResultErrorMessage();
			adPatSearch.getClinicID(Login_Doctor.driver, logger).clear();
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch44() throws Exception {
			logger = Reports.extent.createTest("Verifying the length of alpha numeric values in Clinic ID field");
			input=RandomStringUtils.randomAlphanumeric(40);
			Web_GeneralFunctions.sendkeys(adPatSearch.getClinicID(Login_Doctor.driver, logger), input,"Entering Alpha numeric", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			int alphacount = adPatSearch.getClinicID(Login_Doctor.driver, logger).getAttribute("value").length();
			Assert.assertTrue(alphacount == 30, "Limit of alphanumeric characters added in Clinic id Field is 30");
			adPatSearch.getClinicID(Login_Doctor.driver, logger).clear();
		}
	
	public synchronized void validateNoResultErrorMessage() throws Exception
	{
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		String noResultFound = adPatSearch.getAdvanceSearchNoResultFoundText(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("No results found".equals(noResultFound.trim()),"Actual No Result Found not matched with expected");
	
	}

	@DataProvider(name = "Patient name search data")
	public synchronized String[] getPatientSearchData() {
		String[] s = { "#123*", "*@" };
		return s;
	}

	@DataProvider(name = "Patient search data")
	public synchronized String[] getAgeSearchData() {
		String[] s = { "#*@", "acdfr@#" };
		return s;
	}

}
